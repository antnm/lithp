Require Import String.
Open Scope string_scope.

Inductive LC :=
  | variable    : nat -> LC
  | abstraction : LC  -> LC
  | application : LC  -> LC -> LC.

Scheme Equality for LC.

Notation "M @ N" := (application M N) (at level 19, left associativity).
Notation "\ M"   := (abstraction M) (at level 20, right associativity).
Coercion variable : nat >-> LC.

Inductive LCName :=
  | variableName    : string -> LCName
  | abstractionName : string -> LCName -> LCName
  | applicationName : LCName -> LCName -> LCName.

Notation "M ! N"   := (applicationName M N) (at level 19, left associativity).
Notation "n --> M" := (abstractionName n M) (at level 20, right associativity).
Coercion variableName : string >-> LCName.

Fixpoint getPosition (bound : list string) (x : string) : nat :=
  match bound with
  | nil => 0
  | (cons y rest) => if eqb x y
                     then 0
                     else 1 + (getPosition rest x)
  end.

Fixpoint LCNameToLC (l : LCName) (bound : list string) : LC :=
  match l with
  | variableName n => variable (getPosition bound n)
  | n --> M        => \ (LCNameToLC M (n :: bound))
  | M ! N          => (LCNameToLC M bound) @ (LCNameToLC N bound)
  end.

Close Scope string_scope.

Require Import Nat.

Fixpoint incrementFree (lc : LC) (depth : nat) (i : nat) : LC :=
  match lc with
  | variable n => if n <=? depth
                  then variable n
                  else variable (n + i)
  | \ M => \ (incrementFree M (depth + 1) i)
  | M @ N => (incrementFree M depth i) @ (incrementFree N depth i)
  end.

Fixpoint betaReduction (lc : LC) (x : LC) (depth : nat) : LC :=
  match lc with
  | variable n => if n <=? depth
                  then if n =? depth
                       then (incrementFree x 1 (depth - 1))
                       else variable n
                  else variable (n - 1)
  | \ M => \ (betaReduction M x (depth + 1))
  | M @ N => (betaReduction M x depth) @ (betaReduction N x depth)
  end.

Fixpoint lcEvalFun (l : LC) : LC :=
  match l with
  | variable n => variable n
  | \ M => \ (lcEvalFun M)
  | (\ M) @ N => betaReduction M N 1
  | M @ N => (lcEvalFun M) @ (lcEvalFun N)
  end.

Fixpoint lcEvalN (l : LC) (n : nat) : LC :=
  match n with
  | O => l
  | S n => lcEvalN (lcEvalFun l) n
  end.

Inductive lcEval : LC -> LC -> Prop :=
  | doneP : forall l,
    lcEval l l
  | againP : forall l l',
    lcEval (lcEvalFun l) l' ->
    lcEval l l'.

Definition lcY := \ (\ 2 @ (1 @ 1)) @ (\ 2 @ (1 @ 1)).

Definition lcTrue  := \ \ 2.
Definition lcFalse := \ \ 1.
Definition lcAnd   := \ \ 2 @ 1 @ 2.
Definition lcOr    := \ \ 2 @ 2 @ 1.
Definition lcNot   := \ 1 @ lcFalse @ lcTrue.
Definition lcIfThenElse := \ \ \ 3 @ 2 @ 1.

Definition lcCons := \ \ \ 1 @ 3 @ 2.
Definition lcCar  := \ 1 @ lcTrue.
Definition lcCdr  := \ 1 @ lcFalse.
Definition lcNil  := \ lcTrue.

Definition lcIsZero := \ 1 @ (\ lcFalse) @ lcTrue.

Definition lcZero  := \ \ 1.
Definition lcSucc  := \ \ \ 2 @ (3 @ 2 @ 1).
Definition lcOne   := lcEvalN (lcSucc @ lcZero) 3.
Definition lcTwo   := lcEvalN (lcSucc @ lcOne) 4.
Definition lcThree := lcEvalN (lcSucc @ lcTwo) 5.
Definition lcFour  := lcEvalN (lcSucc @ lcThree) 6.
Definition lcFive  := lcEvalN (lcSucc @ lcFour) 7.
Definition lcSix   := lcEvalN (lcSucc @ lcFive) 8.
Definition lcSeven := lcEvalN (lcSucc @ lcSix) 9.
Definition lcEight := lcEvalN (lcSucc @ lcSeven) 10.
Definition lcNine  := lcEvalN (lcSucc @ lcEight) 11.
Definition lcTen   := lcEvalN (lcSucc @ lcNine) 12.
Definition lcPlus := \ \ 2 @ lcSucc @ 1.
Definition lcMult := \ \ 2 @ (lcPlus @ 1) @ lcZero.
Definition lcPred := \ 1 @ (\ \ lcIsZero @ (2 @ lcOne) @ 1
                                         @ (lcPlus @ (2 @ 1) @ lcOne))
                         @ (\ lcZero) @ lcZero.
Definition lcSub  := \ \ 1 @ lcPred @ 2.

Definition lcLe := \ \ lcIsZero @ (lcSub @ 2 @ 1).
Definition lcLt := \ \ lcNot @ (lcLe @ 1 @ 2).
Definition lcEq := \ \ lcAnd @ (lcLe @ 2 @ 1) @ (lcLe @ 1 @ 2).
Definition lcGt := \ \ lcNot @ (lcLe @ 2 @ 1).
Definition lcGe := \ \ lcLe @ 1 @ 2.

Definition lcDivPair := lcY @ (\ \ \ \ lcLt @ 2 @ 1 @ (lcCons @ 3 @ 2)
                                            @ (4 @ (lcSucc @ 3)
                                                 @ (lcSub @ 2 @ 1) @ 1))
                            @ lcZero.
Definition lcDiv := \ \ lcCar @ (lcDivPair @ 2 @ 1).
Definition lcMod := \ \ lcCdr @ (lcDivPair @ 2 @ 1).

Ltac lcSolve := repeat (first [ eapply doneP | eapply againP ]).

Example exTrue : lcEval lcTrue lcTrue.
  lcSolve. Qed.

Example exSuccZeroIsOne : lcEval (lcSucc @ lcZero) lcOne.
  lcSolve. Qed.

Example exOnePlusOne : lcEval (lcPlus @ lcOne @ lcOne) lcTwo.
  lcSolve. Qed.

Example exOneTimesZero : lcEval (lcMult @ lcOne @ lcZero) lcZero.
  lcSolve. Qed.

Example exPredOne : lcEval (lcPred @ lcOne) lcZero.
  lcSolve. Qed.

Example exTwoMinusOne : lcEval (lcSub @ lcTwo @ lcOne) lcOne.
  lcSolve. Qed.

Example exNotTrue : lcEval (lcNot @ lcTrue) lcFalse.
  lcSolve. Qed.

Example exTrueAndFalse : lcEval (lcAnd @ lcTrue @ lcFalse) lcFalse.
  lcSolve. Qed.

Example exIfTrueThenOneElseZero : lcEval (lcIfThenElse @ lcTrue
                                                       @ lcOne
                                                       @ lcZero)
                                         lcOne.
  lcSolve. Qed.

Example exCar : lcEval (lcCar @ (lcCons @ lcOne @ lcNil)) lcOne.
  lcSolve. Qed.

Example exThreeTimesThree : lcEval (lcMult @ lcThree @ lcThree) lcNine.
  lcSolve. Qed.

Example exCadr : lcEval (lcCar @ (lcCdr @
                         (lcCons @ lcOne @
                         (lcCons @ lcTwo @
                         (lcCons @ lcThree @ lcNil))))) lcTwo.
  lcSolve. Qed.

Example exPredFive : lcEval (lcPred @ lcFive) lcFour.
  lcSolve. Qed.

Example exThreeDividedByTwo : lcEval (lcDiv @ lcThree @ lcTwo) lcOne.
  lcSolve. Qed.

Example exThreeLessThanTwo : lcEval (lcLt @ lcThree @ lcTwo) lcFalse.
  lcSolve. Qed.

Example exThreeEqualToTwo : lcEval (lcEq @ lcThree @ lcTwo) lcFalse.
  lcSolve. Qed.

Example exOneModuloOne : lcEval (lcMod @ lcOne @ lcOne) lcZero.
  lcSolve. Qed.
