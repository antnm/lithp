Require Import String.
Open Scope string_scope.

Require Import BinInt.
Local Open Scope Z_scope.

Inductive Boolean :=
  | t : Boolean
  | f : Boolean.

(* ParameterList must be a flat list *)
Inductive ParameterList :=
  | consParameters   : string -> ParameterList -> ParameterList
  | variadicArgument : string -> ParameterList
  | noParameters     : ParameterList.

Inductive Env :=
  | emptyEnv : Env
  | pushEnv  : string -> nat -> Env -> Env.

Inductive List :=
  | cons          : List      -> List -> List
  | atomProcedure : Procedure -> List
  | atomNumber    : Z         -> List
  | atomSymbol    : string    -> List
  | atomBoolean   : Boolean   -> List
  | emptyList     : List

with Procedure :=
  | procedure : Env -> ParameterList -> TopLevel -> Procedure

with TopLevel :=
  | consExpressions : List -> TopLevel -> TopLevel
  | noExpression    : TopLevel.

Coercion atomNumber  : Z       >-> List.
Coercion atomSymbol  : string  >-> List.
Coercion atomBoolean : Boolean >-> List.

Declare Custom Entry list.
Notation "( x .. y .' z )" := (cons x .. (cons y z) .. )
                              (in custom list at level 1).
Notation "( x .. y )" := (cons x .. (cons y emptyList) .. )
                         (in custom list at level 1).
Notation "()" := emptyList (in custom list at level 0).
Notation "#t" := t (in custom list at level 0).
Notation "#f" := f (in custom list at level 0).
Notation "x" := x (in custom list at level 0,
                   x constr at level 0).

Declare Custom Entry top_level.

Notation "<< l >>" := l (l custom top_level at level 3).
Notation "x" := x (in custom top_level at level 1,
                   x custom list at level 1).
Notation "x .. y" := (consExpressions x .. (consExpressions y noExpression) ..)
                     (in custom top_level at level 1,
                      x custom list at level 1,
                      y custom list at level 1).

Compute << ("define" "x" ("if" #t
                               (1 2 .' 2)
                               (2 .' ("quote" "x"))))
             2
             #t
             ("car" "x") >>.
