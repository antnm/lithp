Add LoadPath "./" as root.
Load syntax.

Fixpoint getAddrEnv (env : Env) (x : string) : option nat :=
  match env with
  | emptyEnv             => None
  | pushEnv x' addr rest => if eqb x x'
                            then Some addr
                            else getAddrEnv rest x
  end.

Inductive Mem :=
  | emptyMem : Mem
  | pushMem  : option List -> Mem -> Mem.

Fixpoint lenMem (mem : Mem) : nat :=
  match mem with
  | emptyMem       => 0
  | pushMem _ rest => 1 + (lenMem rest)
  end.

Fixpoint allocMem (mem : Mem) : Mem :=
  match mem with
  | emptyMem          => pushMem None emptyMem
  | pushMem x    rest => pushMem x (allocMem rest)
  end.

Fixpoint updateMem (mem : Mem) (addr : nat) (x : option List) : Mem :=
  match mem with
  | emptyMem       => mem
  | pushMem y rest => if Nat.eqb addr 0
                      then pushMem x rest
                      else pushMem y (updateMem (rest) (addr - 1) x)
  end.

Fixpoint getMem (mem : Mem) (addr : nat) : option List :=
  match mem, addr with
  | emptyMem,       _   => None
  | pushMem x _,    O   => x
  | pushMem _ rest, S n => getMem rest n
  end.

Inductive Conf :=
  | conf : Env -> Mem -> Conf.

Definition emptyConf := conf emptyEnv emptyMem.

Definition define (c : Conf) (x : string) : Conf :=
  let (env, mem) := c in
  conf (pushEnv x (lenMem mem) env)
       (allocMem mem).

Definition set (c : Conf) (name : string) (val : option List) : option Conf :=
  let (env, mem) := c in
  match getAddrEnv env name with
  | None      => None
  | Some addr => Some (conf env (updateMem mem addr val))
  end.

Definition defineAndSet (c : Conf) (name : string) (val : option List) : Conf :=
  let (env, mem) := c in
  let addr       := lenMem mem in
  conf (pushEnv name addr env)
       (updateMem (allocMem mem) addr val).

Definition get (c : Conf) (name : string) : option List :=
  let (env, mem) := c in
  let addr       := getAddrEnv env name in
  match addr with
  | None      => None
  | Some addr => getMem mem addr
  end.

Fixpoint listToParameters (l : List) : option ParameterList :=
  match l with
  | emptyList    => Some noParameters
  | atomSymbol s => Some (variadicArgument s)
  | cons s rest  => match s, listToParameters rest with
                         | atomSymbol s, Some rest => Some (consParameters s rest)
                         | _,            _         => None
                         end
  | _ => None
  end.

Fixpoint listToTopLevel (l : List) : TopLevel :=
  match l with
  | emptyList          => noExpression
  | cons exp emptyList => consExpressions exp noExpression
  | cons exp rest      => consExpressions exp (listToTopLevel rest)
  | _                  => consExpressions l noExpression
  end.

Definition isAtom (l : List) : Boolean :=
  match l with
  | cons _ _ => f
  | _        => t
  end.

Definition isPair (l : List) : Boolean :=
  match l with
  | cons _ _ => t
  | _        => f
  end.

Definition isBoolean (l : List) : Boolean :=
  match l with
  | atomBoolean _ => t
  | _             => f
  end.

Definition isNumber (l : List) : Boolean :=
  match l with
  | atomNumber _ => t
  | _            => f
  end.

Definition isSymbol (l : List) : Boolean :=
  match l with
  | atomSymbol _ => t
  | _            => f
  end.

Definition isNull (l : List) : Boolean :=
  match l with
  | emptyList => t
  | _         => f
  end.

Definition isUnquote (x : List) (y : List) : Boolean :=
  match x, y with
  | atomSymbol "unquote", (cons y emptyList) => t
  | _,                    _                  => f
  end.

Inductive eval : Conf -> TopLevel -> Conf -> option List -> Prop :=
  | lastExpP : forall x x' env mem env' mem',
    evalExp (conf env mem) x
            (conf env' mem') x' ->
    eval (conf env mem) (consExpressions x noExpression)
         (conf env' mem') x'
  | expP : forall x x' rest rest' env mem env' mem' env'' mem'',
    rest <> noExpression ->
    evalExp (conf env mem) x
            (conf env' mem') x' ->
    eval (conf env' mem') rest
         (conf env'' mem'') rest' ->
    eval (conf env mem) (consExpressions x rest)
         (conf env'' mem'') rest'
  | noExpP : forall c,
    eval c noExpression
         c None

with evalExp : Conf -> List -> Conf -> option List -> Prop :=
  | atomP : forall x c,
    isAtom x = t ->
    evalExp c x
            c (Some x)

  | primitiveP : forall x x' rest c c',
    get c x = None ->
    evalPrim c (cons (atomSymbol x) rest)
             c' x' ->
    evalExp c (cons (atomSymbol x) rest)
            c' x'

  | varP : forall var val c,
    val = get c var ->
    evalExp c var
            c val

  | lambdaCallP : forall lambda params args seq result 
                         c env mem env' mem' env'' env''' mem''',
    evalExp (conf env mem) lambda
            (conf env' mem')
            (Some (atomProcedure (procedure env'' params seq))) ->
    pushArgs (conf env mem') (procedure env'' params seq) args c ->
    eval c seq
         (conf env''' mem''') result ->
    evalExp (conf env mem) (cons lambda args)
            (conf env mem''') result

with evalPrim : Conf -> List -> Conf -> option List -> Prop :=
  | lambdaP : forall env mem params params' seq,
    Some (params') = listToParameters params ->
    evalPrim (conf env mem) (cons "lambda" (cons params seq))
             (conf env mem)
             (Some (atomProcedure (procedure env params' (listToTopLevel seq))))

  | quoteP : forall x c,
    evalPrim c (cons "quote" (cons x emptyList))
             c (Some x)
  | quasiquoteP : forall x env mem env' mem' result,
    quasiquoteEval (conf env mem) x
                   (conf env' mem') result ->
    evalPrim (conf env mem) (cons "quasiquote" (cons x emptyList))
             (conf env mem') (Some result)

  | beginP : forall env env' mem mem' seq result,
    eval (conf env mem) (listToTopLevel seq)
         (conf env' mem') result ->
    evalPrim (conf env mem) (cons "begin" seq)
             (conf env mem') result

  | ifTrueP : forall env mem env' mem' env'' mem''
                     condition consequent alternative result,
    evalExp (conf env mem) condition
            (conf env' mem') (Some (atomBoolean t)) ->
    evalExp (conf env mem') consequent
            (conf env'' mem'') result ->
    evalPrim (conf env mem) (cons "if" (cons condition
                              (cons consequent
                              (cons alternative emptyList))))
             (conf env mem'') result

  | ifFalseP : forall env mem env' mem' env'' mem''
                      condition consequent alternative result,
    evalExp (conf env mem) condition
            (conf env' mem') (Some (atomBoolean f)) ->
    evalExp (conf env mem') alternative
            (conf env'' mem'') result ->
    evalPrim (conf env mem) (cons "if" (cons condition
                              (cons consequent
                              (cons alternative emptyList))))
             (conf env mem'') result

  | whileTrueP : forall env mem env' mem' env'' mem'' env''' mem'''
                        seq cond result x,
    evalExp (conf env mem) cond
            (conf env' mem') (Some (atomBoolean t)) ->
    eval (conf env mem') (listToTopLevel seq)
         (conf env'' mem'') x ->
    evalPrim (conf env mem'') (cons "while" (cons cond seq))
             (conf env''' mem''') result ->
    evalPrim (conf env mem) (cons "while" (cons cond seq))
             (conf env mem''') result
  | whileFalseP : forall env mem env' mem' seq cond,
    evalExp (conf env mem) cond
            (conf env' mem') (Some (atomBoolean f)) ->
    evalPrim (conf env mem) (cons "while" (cons cond seq))
             (conf env mem') (Some (atomBoolean f))

  | letNothingP : forall env env' mem mem' result seq,
    eval (conf env mem) (listToTopLevel seq)
         (conf env' mem') result ->
    evalPrim (conf env mem) (cons "let" (cons emptyList seq))
             (conf env mem') result
  | letP : forall c env mem env' mem' exp exp' seq s restBinds result,
    evalExp (conf env mem) exp
            c exp' ->
    evalPrim (defineAndSet c s exp') (cons "let" (cons restBinds seq))
             (conf env' mem') result ->
    evalPrim (conf env mem) (cons "let" (cons (cons (cons (atomSymbol s)
                                                          (cons exp emptyList))
                                                    restBinds)
                                              seq))
             (conf env mem') result

  | setP : forall var val val' c env mem env' mem',
    evalExp (conf env mem) val
            (conf env' mem') val' ->
    Some c = set (conf env mem') var val' ->
    evalPrim (conf env mem) (cons "set!" (cons var (cons val emptyList)))
             c None

  | defineNothingP : forall var c c',
    c' = define c var ->
    evalPrim c (cons "define" (cons var emptyList))
             c' None
  | defineP : forall c c' c'' var val val',
    evalExp (define c var) val
            c' val' ->
    Some c'' = set c' var val' ->
    evalPrim c (cons "define" (cons (atomSymbol var)
                                    (cons val emptyList)))
             c'' None

  | carP : forall x rest result env mem env' mem',
    evalExp (conf env mem) x
            (conf env' mem') (Some (cons result rest)) ->
    evalPrim (conf env mem) (cons "car" (cons x emptyList))
             (conf env mem') (Some result)
  | cdrP : forall x first result env mem env' mem',
    evalExp (conf env mem) x
            (conf env' mem') (Some (cons first result)) ->
    evalPrim (conf env mem) (cons "cdr" (cons x emptyList))
             (conf env mem') (Some result)

  | plusNothingP : forall c,
    evalPrim c (cons "+" emptyList)
             c (Some (atomNumber 0))
  | plusP : forall x x' rest rest' result env mem env' mem' env'' mem'',
    evalExp (conf env mem) x
            (conf env' mem') (Some (atomNumber x')) ->
    evalPrim (conf env mem') (cons "+" rest)
             (conf env'' mem'') (Some (atomNumber rest')) ->
    result = x' + rest' ->
    evalPrim (conf env mem) (cons "+" (cons x rest))
             (conf env mem'') (Some (atomNumber result))

  | minusAtomP : forall x x' x'' env mem env' mem',
    evalExp (conf env mem) x
            (conf env' mem') (Some (atomNumber x')) ->
    x'' = -x' ->
    evalPrim (conf env mem) (cons "-" (cons x emptyList))
             (conf env mem') (Some (atomNumber x''))
  | minusP : forall x x' rest rest' result env mem env' mem' env'' mem'',
    rest <> emptyList ->
    evalExp (conf env mem) x
            (conf env' mem') (Some (atomNumber x')) ->
    evalPrim (conf env mem') (cons "+" rest)
             (conf env'' mem'') (Some (atomNumber rest')) ->
    result = x' - rest' ->
    evalPrim (conf env mem) (cons "-" (cons x rest))
             (conf env mem'') (Some (atomNumber result))

  | timesNothingP : forall c,
    evalPrim c (cons "*" emptyList)
             c (Some (atomNumber 1))
  | timesP : forall x x' rest rest' result env mem env' mem' env'' mem'',
    evalExp (conf env mem) x
            (conf env' mem') (Some (atomNumber x')) ->
    evalPrim (conf env mem') (cons "*" rest)
             (conf env'' mem'') (Some (atomNumber rest')) ->
    result = x' * rest' ->
    evalPrim (conf env mem) (cons "*" (cons x rest))
             (conf env mem'') (Some (atomNumber result))

  | divideAtomP : forall x x' env mem env' mem',
    evalExp (conf env mem) x
            (conf env' mem') (Some (atomNumber x')) ->
    evalPrim (conf env mem) (cons "/" (cons x emptyList))
             (conf env mem') (Some (atomNumber x'))
  | divideP : forall x x' rest rest' result env mem env' mem' env'' mem'',
    evalExp (conf env mem) x
            (conf env' mem') (Some (atomNumber x')) ->
    evalPrim (conf env mem') (cons "*" rest)
             (conf env'' mem'') (Some (atomNumber rest')) ->
    result = x' / rest' ->
    evalPrim (conf env mem) (cons "/" (cons x rest))
             (conf env mem'') (Some (atomNumber result))

  | moduloP : forall x x' y y' result env mem env' mem' env'' mem'',
    evalExp (conf env mem) x
            (conf env' mem') (Some (atomNumber x')) ->
    evalExp (conf env mem') y
            (conf env'' mem'') (Some (atomNumber y')) ->
    result = Z.modulo x' y' ->
    evalPrim (conf env mem) (cons "modulo" (cons x (cons y emptyList)))
             (conf env mem'') (Some (atomNumber result))

  | remainderP : forall x x' y y' result env mem env' mem' env'' mem'',
    evalExp (conf env mem) x
            (conf env' mem') (Some (atomNumber x')) ->
    evalExp (conf env mem') y
            (conf env'' mem'') (Some (atomNumber y')) ->
    result = Z.rem x' y' ->
    evalPrim (conf env mem) (cons "remainder" (cons x (cons y emptyList)))
             (conf env mem'') (Some (atomNumber result))

  | isNumberP : forall x e result env mem env' mem',
    evalExp (conf env mem) x
            (conf env' mem') (Some e) ->
    result = isNumber e ->
    evalPrim (conf env mem) (cons "number?" (cons x emptyList))
             (conf env mem') (Some (atomBoolean result))

  | isBooleanP : forall x e result env mem env' mem',
    evalExp (conf env mem) x
            (conf env' mem') (Some e) ->
    result = isBoolean e ->
    evalPrim (conf env mem) (cons "boolean?" (cons x emptyList))
             (conf env mem') (Some (atomBoolean result))

  | isSymbolP : forall x e result env mem env' mem',
    evalExp (conf env mem) x
            (conf env' mem') (Some e) ->
    result = isSymbol e ->
    evalPrim (conf env mem) (cons "symbol?" (cons x emptyList))
             (conf env mem') (Some (atomBoolean result))

  | isPairP : forall x e result env mem env' mem',
    evalExp (conf env mem) x
            (conf env' mem') (Some e) ->
    result = isPair e ->
    evalPrim (conf env mem) (cons "pair?" (cons x emptyList))
             (conf env mem') (Some (atomBoolean result))

  | isNullP : forall x e result env mem env' mem',
    evalExp (conf env mem) x
            (conf env' mem') (Some e) ->
    result = isNull e ->
    evalPrim (conf env mem) (cons "null?" (cons x emptyList))
             (conf env mem') (Some (atomBoolean result))

  | eqNothingP : forall c,
    evalPrim c (cons "=" emptyList)
             c (Some (atomBoolean t))
  | eqAtomP : forall x c,
    evalPrim c (cons "=" (cons x emptyList))
             c (Some (atomBoolean t))
  | eqFalseP : forall x x' y y' rest env mem env' mem' env'' mem'',
    evalExp (conf env mem) x
            (conf env' mem') (Some (atomNumber x')) ->
    evalExp (conf env mem') y
            (conf env'' mem'') (Some (atomNumber y')) ->
    x' <> y' ->
    evalPrim (conf env mem) (cons "=" (cons x (cons y rest)))
             (conf env mem'') (Some (atomBoolean f))
  | eqTrueP : forall x x' y rest result
              env mem env' mem' env'' mem'' env''' mem''',
    evalExp (conf env mem) x
            (conf env' mem') (Some (atomNumber x')) ->
    evalExp (conf env mem') y
            (conf env'' mem'') (Some (atomNumber x')) ->
    evalPrim (conf env mem'') (cons "=" (cons y rest))
             (conf env''' mem''') result ->
    evalPrim (conf env mem) (cons "=" (cons x (cons y rest)))
             (conf env''' mem''') result

  | gtNothingP : forall c,
    evalPrim c (cons ">" emptyList)
             c (Some (atomBoolean t))
  | gtAtomP : forall x c,
    evalPrim c (cons ">" (cons x emptyList))
             c (Some (atomBoolean t))
  | gtFalseP : forall x x' y y' rest env mem env' mem' env'' mem'',
    evalExp (conf env mem) x
            (conf env' mem') (Some (atomNumber x')) ->
    evalExp (conf env mem') y
            (conf env'' mem'') (Some (atomNumber y')) ->
    x' <= y' ->
    evalPrim (conf env mem) (cons ">" (cons x (cons y rest)))
             (conf env mem'') (Some (atomBoolean f))
  | gtTrueP : forall x x' y y' rest result
                     env mem env' mem' env'' mem'' env''' mem''',
    evalExp (conf env mem) x
            (conf env' mem') (Some (atomNumber x')) ->
    evalExp (conf env mem') y
            (conf env'' mem'') (Some (atomNumber y')) ->
    x' > y' ->
    evalPrim (conf env mem'') (cons ">" (cons y rest))
             (conf env''' mem''') result ->
    evalPrim (conf env mem) (cons ">" (cons x (cons y rest)))
             (conf env mem''') result

  | ltNothingP : forall c,
    evalPrim c (cons "<" emptyList)
             c (Some (atomBoolean t))
  | ltAtomP : forall x c,
    evalPrim c (cons "<" (cons x emptyList))
             c (Some (atomBoolean t))
  | ltFalseP : forall x x' y y' rest env mem env' mem' env'' mem'',
    evalExp (conf env mem) x
            (conf env' mem') (Some (atomNumber x')) ->
    evalExp (conf env mem') y
            (conf env'' mem'') (Some (atomNumber y')) ->
    x' >= y' ->
    evalPrim (conf env mem) (cons "<" (cons x (cons y rest)))
             (conf env mem'') (Some (atomBoolean f))
  | ltTrueP : forall x x' y y' rest result 
                     env mem env' mem' env'' mem'' env''' mem''',
    evalExp (conf env mem) x
            (conf env' mem') (Some (atomNumber x')) ->
    evalExp (conf env mem') y
            (conf env'' mem'') (Some (atomNumber y')) ->
    x' < y' ->
    evalPrim (conf env mem'') (cons "<" (cons y rest))
             (conf env''' mem''') result ->
    evalPrim (conf env mem) (cons "<" (cons x (cons y rest)))
             (conf env mem''') result

  | gteNothingP : forall c,
    evalPrim c (cons ">=" emptyList)
             c (Some (atomBoolean t))
  | gteAtomP : forall x c,
    evalPrim c (cons ">=" (cons x emptyList))
             c (Some (atomBoolean t))
  | gteFalseP : forall x x' y y' rest env mem env' mem' env'' mem'',
    evalExp (conf env mem) x
            (conf env' mem') (Some (atomNumber x')) ->
    evalExp (conf env mem') y
            (conf env'' mem'') (Some (atomNumber y')) ->
    x' < y' ->
    evalPrim (conf env mem) (cons ">=" (cons x (cons y rest)))
             (conf env mem'') (Some (atomBoolean f))
  | gteTrueP : forall x x' y y' rest result 
                      env mem env' mem' env'' mem'' env''' mem''',
    evalExp (conf env mem) x
            (conf env' mem') (Some (atomNumber x')) ->
    evalExp (conf env mem') y
            (conf env'' mem'') (Some (atomNumber y')) ->
    x' >= y' ->
    evalPrim (conf env mem'') (cons ">=" (cons y rest))
             (conf env''' mem''') result ->
    evalPrim (conf env mem) (cons ">=" (cons x (cons y rest)))
             (conf env mem''') result

  | lteNothingP : forall c,
    evalPrim c (cons "<=" emptyList)
             c (Some (atomBoolean t))
  | lteAtomP : forall x c,
    evalPrim c (cons "<=" (cons x emptyList))
             c (Some (atomBoolean t))
  | lteFalseP : forall x x' y y' rest env mem env' mem' env'' mem'',
    evalExp (conf env mem) x
            (conf env' mem') (Some (atomNumber x')) ->
    evalExp (conf env mem') y
            (conf env'' mem'') (Some (atomNumber y')) ->
    x' > y' ->
    evalPrim (conf env mem) (cons "<=" (cons x (cons y rest)))
             (conf env mem'') (Some (atomBoolean f))
  | lteTrueP : forall x x' y y' rest result 
                      env mem env' mem' env'' mem'' env''' mem''',
    evalExp (conf env mem) x
            (conf env' mem') (Some (atomNumber x')) ->
    evalExp (conf env mem') y
            (conf env'' mem'') (Some (atomNumber y')) ->
    x' <= y' ->
    evalPrim (conf env mem'') (cons "<=" (cons y rest))
             (conf env''' mem''') result ->
    evalPrim (conf env mem) (cons "<=" (cons x (cons y rest)))
             (conf env mem''') result

  | andNothingP : forall c,
    evalPrim c (cons "and" emptyList)
             c (Some (atomBoolean t))
  | andTrueP : forall x x' rest result env mem env' mem' env'' mem'',
    evalExp (conf env mem) x
            (conf env' mem') (Some x') ->
    x' <> f ->
    evalPrim (conf env mem') (cons "and" rest)
             (conf env'' mem'') result ->
    evalPrim (conf env mem) (cons "and" (cons x rest))
             (conf env mem'') result
  | andFalseP : forall x rest env mem env' mem',
    evalExp (conf env mem) x
            (conf env' mem') (Some (atomBoolean f)) ->
    evalPrim (conf env mem) (cons "and" (cons x rest))
             (conf env mem') (Some (atomBoolean f))

  | orNothingP : forall c,
    evalPrim c (cons "or" emptyList)
             c (Some (atomBoolean f))
  | orFalseP : forall x rest result env mem env' mem' env'' mem'',
    evalExp (conf env mem) x
            (conf env' mem') (Some (atomBoolean f)) ->
    evalPrim (conf env mem') (cons "or" rest)
             (conf env'' mem'') result ->
    evalPrim (conf env mem) (cons "or" (cons x rest))
             (conf env mem'') result
  | orTrueP : forall x x' rest env mem env' mem',
    evalExp (conf env mem) x
            (conf env' mem') (Some x') ->
    x' <> f ->
    evalPrim (conf env mem) (cons "or" (cons x rest))
             (conf env mem') (Some (atomBoolean t))

  | notTrueP : forall x x' env mem env' mem',
    evalExp (conf env mem) x
            (conf env' mem') (Some x') ->
    x' <> f ->
    evalPrim (conf env mem) (cons "not" (cons x emptyList))
             (conf env mem') (Some (atomBoolean f))
  | notFalseP : forall x env mem env' mem',
    evalExp (conf env mem) x
            (conf env' mem') (Some (atomBoolean t)) ->
    evalPrim (conf env mem) (cons "not" (cons x emptyList))
             (conf env' mem') (Some (atomBoolean t))

with quasiquoteEval : Conf -> List -> Conf -> List -> Prop :=
  | quasiquoteAtomP : forall x c,
    isAtom x = t ->
    quasiquoteEval c x
                   c x
  | unquoteP : forall x result env mem env' mem',
    evalExp (conf env mem) x
            (conf env' mem') (Some result) ->
    quasiquoteEval (conf env mem) (cons "unquote" (cons x emptyList))
                   (conf env mem') result
  | quasiquotePairP : forall x x' y y' env mem env' mem' env'' mem'',
    isUnquote x y = f ->
    quasiquoteEval (conf env mem) x
                   (conf env' mem') x' ->
    quasiquoteEval (conf env mem') y
                   (conf env'' mem'') y' ->
    quasiquoteEval (conf env mem) (cons x y)
                   (conf env mem'') (cons x' y')

with pushArgs : Conf -> Procedure -> List -> Conf -> Prop :=
  | noArgsP : forall env mem env' seq ,
    pushArgs (conf env mem) (procedure env' noParameters seq)
             emptyList (conf env' mem)
  | varArgEmptyP : forall mem env env' seq name,
    pushArgs (conf env mem) (procedure env' (variadicArgument name) seq)
             emptyList (defineAndSet (conf env' mem) name (Some emptyList))
  | varArgP : forall seq arg arg' rest rest' name
                     c procEnv env mem env' mem' env'' mem'',
    evalExp (conf env mem) arg
            (conf env' mem') (Some arg') ->
    pushArgs (conf env mem') (procedure procEnv (variadicArgument name) seq)
             rest (conf env'' mem'') ->
    Some rest' = get (conf env'' mem'') name ->
    Some c = set (conf env'' mem'') name (Some (cons arg' rest')) ->
    pushArgs (conf env mem) (procedure procEnv (variadicArgument name) seq)
             (cons arg rest) c
  | argsP : forall seq arg arg' name restArgs restParams
                   c env mem env' mem' procEnv,
    evalExp (conf env mem) arg
            (conf env' mem') arg' ->
    pushArgs (conf env mem') (procedure procEnv restParams seq) restArgs c ->
    pushArgs (conf env mem)
             (procedure procEnv (consParameters name restParams) seq)
             (cons arg restArgs) (defineAndSet c name arg').

Require Import Lia.

Create HintDb lithpHints.
Hint Constructors eval : lithpHints.
Hint Constructors evalExp : lithpHints.
Hint Constructors evalPrim : lithpHints.
Hint Constructors quasiquoteEval : lithpHints.
Hint Constructors pushArgs : lithpHints.
Hint Extern 1 => discriminate : lithpHints.
Hint Extern 1 => reflexivity : lithpHints.
Hint Extern 1 => lia : lithpHints.

Example exNumber : exists conf,
                   eval emptyConf << ("number?" 2) >>
                        conf (Some (atomBoolean t)).
Proof.
  eauto 6 with lithpHints.
Qed.

Example exNumber2 : exists conf,
                    eval emptyConf << ("number?" #t) >>
                         conf (Some (atomBoolean f)).
Proof.
  eauto 6 with lithpHints.
Qed.

Example exSymbol : exists conf,
                   eval emptyConf << ("symbol?" ("quote" "x")) >>
                        conf (Some (atomBoolean t)).
Proof.
  eauto 6 with lithpHints.
Qed.

Example exPlus : exists conf,
                 eval emptyConf << ("+" 2) >>
                      conf (Some (atomNumber 2)).
Proof.
  eauto 6 with lithpHints.
Qed.

Example exPlus2 : exists conf,
                  eval emptyConf << ("+" 2 3 4) >>
                       conf (Some (atomNumber 9)).
Proof.
  eauto 10 with lithpHints.
Qed.

Example exMinus : exists conf,
                  eval emptyConf << ("-" ("-" 2)) >>
                       conf (Some (atomNumber 2)).
Proof.
  eauto 8 with lithpHints.
Qed.

Example exMinus2 : exists conf,
                   eval emptyConf << ("-" 2 2 2) >>
                        conf (Some (atomNumber (-2))).
Proof.
  eauto 10 with lithpHints.
Qed.

Example exTimesDivide : exists conf,
                        eval emptyConf << ("/" ("*" 6 4) 3) >>
                             conf (Some (atomNumber 8)).
Proof.
  eauto 12 with lithpHints.
Qed.

Example exMod : exists conf,
                eval emptyConf << ("modulo" ("-" 2) 3) >>
                     conf (Some (atomNumber 1)).
Proof.
  eauto 9 with lithpHints.
Qed.

Example exRem : exists conf,
                eval emptyConf << ("remainder" ("-" 2) 3) >>
                     conf (Some (atomNumber (-2))).
Proof.
  eauto 9 with lithpHints.
Qed.

Example exLteAndGt : exists conf,
                     eval emptyConf << ("and" ("<=" 2 2 3)
                                              ("=" 2 2 3)) >>
                     conf (Some (atomBoolean f)).
Proof.
  eauto 20 with lithpHints.
Qed.

Example exPair : exists conf,
                 eval emptyConf << ("pair?" ("quote" (("quote" (2 4 5)) .' 3))) >>
                      conf (Some (atomBoolean t)).
Proof.
  eauto 6 with lithpHints.
Qed.

Example exCons : exists conf,
                 eval emptyConf << ("quote" (2 .' (1 2 3))) >>
                      conf (Some (cons 2 (cons 1 (cons 2 (cons 3 emptyList))))).
Proof.
  eauto 4 with lithpHints.
Qed.

Example exCar : exists conf,
                eval emptyConf << ("car" ("quote" (2 .' 3))) >>
                     conf (Some (atomNumber 2)).
Proof.
  eauto 6 with lithpHints.
Qed.

Example exCdr : exists conf,
                eval emptyConf << ("cdr" ("quote" (2 .' 3))) >>
                     conf (Some (atomNumber 3)).
Proof.
  eauto 6 with lithpHints.
Qed.

Example exBegin : exists conf,
                  eval emptyConf << ("begin"
                                     2
                                     3
                                     ("=")) >>
                       conf (Some (atomBoolean t)).
Proof.
  eexists. eapply lastExpP. apply primitiveP; eauto 1 with lithpHints.
  eapply beginP. eapply expP; eauto 5 with lithpHints.
Qed.

Example exSet : exists conf,
                eval emptyConf << ("define" "x" 2)
                                  ("begin"
                                    ("set!" "x" 3))
                                  "x" >>
                conf (Some (atomNumber 3)).
Proof.
  eexists. eapply expP; eauto 10 with lithpHints.
Qed.

Example exRestore : exists conf,
                    eval emptyConf << ("define" "x" 2)
                                      ("begin"
                                        ("define" "x" 3))
                                      "x" >>
                    conf (Some (atomNumber 2)).
Proof.
  eexists. eapply expP; eauto 10 with lithpHints.
Qed.

Example exIdentity : exists conf,
                     eval emptyConf << (("lambda" ("x") "x") 2) >>
                     conf (Some (atomNumber 2)).
Proof.
  eauto 10 with lithpHints.
Qed.

Example exNoVarArgs : exists conf,
                      eval emptyConf << (("lambda" "x" "x")) >>
                      conf (Some emptyList).
Proof.
  eauto 8 with lithpHints.
Qed.

Example exFirst : exists conf,
                  eval emptyConf << (("lambda" ("a" .' "x") ("car" "x")) 0 1 2 3) >>
                  conf (Some (atomNumber 1)).
Proof.
  eauto 18 with lithpHints.
Qed.

Example exFactorial : exists conf,
                      eval emptyConf <<
                      ("define" "factorial"
                         ("lambda" ("x")
                            ("if" ("<=" "x" 1)
                               1
                               ("*" "x"
                                 ("factorial" ("-" "x" 1))))))
                      ("factorial" 3)
                      >> conf (Some (atomNumber 6)).
Proof.
  eexists. eapply expP; eauto 52 with lithpHints.
Qed.

Example exLet : exists conf,
                eval emptyConf <<
                ("let" (("x" ("+" 2 3))
                        ("y" 10))
                   ("+" "x" "y"))
                >> conf (Some (atomNumber 15)).
Proof.
  eexists. apply lastExpP. eapply primitiveP; eauto 1 with lithpHints.
  eapply letP; eauto 6 with lithpHints.
  eapply letP; eauto 6 with lithpHints.
  eapply letNothingP. eauto 7 with lithpHints.
Qed.

Example exClosure : exists conf,
                    eval emptyConf <<
                    ("define" "x" 2)
                    ("define" "f"
                       ("lambda" () "x"))
                    ("define" "x" 3)
                    ("f")
                    >> conf (Some (atomNumber 2)).
Proof.
  eexists. eapply expP; eauto 15 with lithpHints.
Qed.

Example exQuasiquote : exists conf,
                       eval emptyConf <<
                       ("quasiquote" (1 2 ("unquote" ("+" 2 3))))
                       >> conf (Some (cons 1 (cons 2 (cons 5 emptyList)))).
Proof.
  eauto 17 with lithpHints.
Qed.

Example exWhile : exists conf,
                  eval emptyConf <<
                  ("define" "x" 0)
                  ("while" ("<" "x" 2)
                    ("set!" "x" ("+" "x" 1)))
                  "x"
                  >> conf (Some (atomNumber 2)).
Proof.
  eexists. eapply expP; eauto 36 with lithpHints.
Qed.

Example exShadowPrimitive : exists conf,
                            eval emptyConf <<
                            ("define" "define" 1)
                            "define"
                            >> conf (Some (atomNumber 1)).
Proof.
  eexists. eapply expP; eauto 4 with lithpHints.
Qed.